package org.opentele.server.citizen.medicinelist

import grails.buildtestdata.mixin.Build
import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.opentele.server.model.Document
import org.opentele.server.model.DocumentCategory
import org.opentele.server.model.Patient
import org.opentele.server.model.User
import spock.lang.Specification

@TestFor(MedicineListService)
@Build([Patient, User, Document])
@Mock([Patient, User, Document])
class MedicineListServiceSpec extends Specification {

    def patient
    def mockSpringSecurityService

    def setup() {

        mockSpringSecurityService = mockFor(SpringSecurityService)
        mockSpringSecurityService.metaClass.encodePassword = {pw ->
            return pw
        }
        patient = Patient.build()
        patient.user = User.build(springSecurityService: mockSpringSecurityService).save(failOnError: true)
        patient.save(failOnError: true)

        mockSpringSecurityService.metaClass.getCurrentUser = { ->
            patient.getUser()
        }
        service.springSecurityService = mockSpringSecurityService
    }

    def "can determine that patient does not have a medicine list" () {
        when:
        def hasMedicineList = service.existsForPatient()

        then:
        hasMedicineList == false;
    }

    def "can determine that patient has a medicine list"() {
        given:
        Document.build(patient: patient, category: DocumentCategory.MEDICINE_LIST).save(failOnError: true)

        when:
        def hasMedicineList = service.existsForPatient()

        then:
        hasMedicineList == true;
    }

    def "can get medicine list for patient"() {
        given:
        def doc = Document.build(patient: patient, category: DocumentCategory.MEDICINE_LIST).save(failOnError: true)

        when:
        def medicineList = service.getForPatient()

        then:
        medicineList == doc
    }

    def "returns null when patient has no medicine list"() {
        when:
        def medicineList = service.getForPatient()

        then:
        medicineList == null
    }
//
//    RA: Due to bug described in KIH-1818 and hack implemented in markAsRead method (the document.refresh() part) this unit test can not execute as grails unit test fails when refreshing,
//    yet another Grails bug...Reintroduce this test when the real problem is fixed or add integration test instead.
//    def "can mark document as read"() {
//        given:
//        def doc = Document.build(isRead: false, version: 1, patient: patient, category: DocumentCategory.MEDICINE_LIST).save(failOnError: true)
//
//        when:
//        service.markAsRead(Document.get(doc.id))
//
//        then:
//        Document.get(doc.id).isRead == true
//    }
}
