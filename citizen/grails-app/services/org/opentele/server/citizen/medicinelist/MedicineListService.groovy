package org.opentele.server.citizen.medicinelist

import org.opentele.server.model.Document
import org.opentele.server.model.DocumentCategory
import org.opentele.server.model.Patient

class MedicineListService {

    def springSecurityService

    def existsForPatient() {

        return getForPatient() != null
    }

    def getForPatient() {

        def user = springSecurityService.currentUser
        def patient = Patient.findByUser(user)

        return Document.findByPatientAndCategory(patient, DocumentCategory.MEDICINE_LIST)
    }

    def markAsRead(Document document) {

        def expectedVersion = document.version
        document.refresh()
        if (expectedVersion != document.version) { // to detect problem related to KIH-1818
            log.warn("Old version of Document passed to the markAsRead method. Version passed: $expectedVersion, actual version: ${document.version}")
        }

        if (document.isRead == false) {
            document.isRead = true
            document.save()
        }
    }
}
